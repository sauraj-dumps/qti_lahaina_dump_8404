## qssi-user 13 TP1A.220905.001 1697717648897 release-keys
- Manufacturer: qualcomm
- Platform: lahaina
- Codename: lahaina
- Brand: qti
- Flavor: qssi-user
- Release Version: 13
- Kernel Version: 5.4.233
- Id: TP1A.220905.001
- Incremental: 1697717648897
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: zh-CN
- Screen Density: undefined
- Fingerprint: qti/lahaina/lahaina:12/RKQ1.211119.001/1697719655101:user/release-keys
- OTA version: 
- Branch: qssi-user-13-TP1A.220905.001-1697717648897-release-keys
- Repo: qti_lahaina_dump_8404
